import "./App.css";
import Home from "./components/Home";
import MenuBar from "./components/MenuBar";
import Video from "./components/Video";
import Account from "./components/Account";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container, Row } from "react-bootstrap";
import React, { useState } from "react";
import HomeData from "./components/HomeData";
import Welcome from "./components/Welcome";
import Auth from "./components/Auth";
import ProtectRoute from "./components/ProtectRoute";

function App() {

  const [anime, setAnime] = useState([
    {
      id: 1,
      title: "Hotaru",
      thumbnail: "/images/hotaru.jpg",
      description: "content",
    },
    {
      id: 2,
      title: "Living with Dying",
      thumbnail: "/images/I_want.jpg",
      description: "content",
    },
    {
      id: 3,
      title: "Grave of Fireflies",
      thumbnail: "/images/grave_of_fireflies.jpg",
      description: "content",
    },
    {
      id: 4,
      title: "5 Centimeter",
      thumbnail: "/images/5-centimeter.png",
      description: "content",
    },
    {
      id: 5,
      title: "Demon Slayer",
      thumbnail: "/images/demon_slayer.png",
      description: "content",
    },
  ]);

  const [isSignin, setSignIn] = useState(false)

  function onSignin() {
    setSignIn(!isSignin)
  }

  return (
    <>
      <Router>
        <MenuBar />
        <Container>
          <Switch>
            <Route exact path="/">
              <Row style={{ marginTop: 20 }}> <Home anime={anime} /></Row>
            </Route>
            <Route path="/homedata/:id" component={HomeData} />
            <Route path="/video" component={Video} />
            <Route path="/Account" component={Account} />
            <ProtectRoute isSignin={isSignin} path="/welcome" component={() => <Welcome onSignin={onSignin} isSignin={isSignin} />} exact />
            <Route render={() => <Auth onSignin={onSignin} isSignin={isSignin} />} />
          </Switch>
        </Container>
      </Router>
    </>
  );
}

export default App;
