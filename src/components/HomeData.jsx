import React from 'react'
import { useParams } from 'react-router-dom'

export default function HomeData() {
    
    let param = useParams();    
  console.log("Param: ", param)
    return (
        <div>
            <h2>Detail {param.id}</h2>
        </div>
    )
}
