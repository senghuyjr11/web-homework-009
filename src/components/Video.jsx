import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Row, Col } from "react-bootstrap";
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
} from "react-router-dom";

export default function Video() {
  return (
    <>
      <Router>
        <h2>Video</h2>
        <br />
        <Link to="/anime">
          <Button variant="primary">Anime</Button>
        </Link>
        <Link to="/movie">
          <Button variant="primary">Movie</Button>
        </Link>

        <Switch>
          <Route path="/anime">
            <Anime />
          </Route>
          <Route path="/movie">
            <Movie />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

function Anime() {
  let { path, url } = useRouteMatch();
  return (
    <div>
      <h2>Anime Category</h2>
      <Link to={`${url}/Adventure`}>
        <Button variant="secondary">Adventure</Button>
      </Link>
      <Link to={`${url}/Action`}>
        <Button variant="secondary">Action</Button>
      </Link>
      <Link to={`${url}/Sad Ending`}>
        <Button variant="secondary">Sad Ending</Button>
      </Link>
      <Link to={`${url}/Based on Reality`}>
        <Button variant="secondary">Based on Reality</Button>
      </Link><br />
      <Row style={{ marginLeft: 5 }}>
        <h4>Please choose a topic: </h4>
        <Switch>
          <Route path={`${path}/:topicId`}>
            <Topic />
          </Route>
        </Switch>
      </Row>
    </div>
  );
}

function Movie() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Movie Category</h2>
      <Link to={`${url}/Adventure`}>
        <Button variant="secondary">Adventure</Button>
      </Link>
      <Link to={`${url}/Crime`}>
        <Button variant="secondary">Crime</Button>
      </Link>
      <Link to={`${url}/Action`}>
        <Button variant="secondary">Action</Button>
      </Link>
      <Link to={`${url}/Romance`}>
        <Button variant="secondary">Romance</Button>
      </Link>
      <Link to={`${url}/Comedy`}>
        <Button variant="secondary">Comedy</Button>
      </Link><br />
      <Row style={{ marginLeft: 4 }}>
        <h4>Please choose a topic: </h4>
        <Switch>
          <Route path={`${path}/:topicId`}>
            <Topic />
          </Route>
        </Switch>
      </Row>
    </div>
  );
}

function Topic() {
  let { topicId } = useParams();
  return (
    <>
      <h4>
        <Col><span style={{ color: "red" }}>{topicId}</span></Col>
      </h4>
    </>
  );
}
