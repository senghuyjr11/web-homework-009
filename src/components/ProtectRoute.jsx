import React, {Component} from 'react';
import { Redirect } from 'react-router';

export default function ProtectRoute({isSignin, component: Component, ...rest}) {
    if(isSignin){
        return <Component {...rest}/>
    }else {
        return <Redirect to="/" />
    }
}