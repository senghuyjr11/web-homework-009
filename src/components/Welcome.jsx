import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Button } from "react-bootstrap";

export default function Welcome(props) {
    return (
        <div>
            <Router>
                <h2>Welcome</h2>
                <Button onClick={props.onSignin} variant="outline-success">{props.isSignin ? "Sign Out" : "Sign Out"}</Button>
            </Router>
        </div>
    )
}
